data "aws_caller_identity" "default" {}

data "aws_region" "default" {}

locals {
  aws_region     = "${signum(length(var.aws_region)) == 1 ? var.aws_region : data.aws_region.default.name}"
  aws_account_id = "${signum(length(var.aws_account_id)) == 1 ? var.aws_account_id : data.aws_caller_identity.default.account_id}"
}

# Define composite variables for resources
module "label" {
  source     = "git::https://github.com/cloudposse/terraform-terraform-label.git?ref=tags/0.1.6"
  namespace  = "${var.namespace}"
  name       = "${var.name}"
  stage      = "${var.stage}"
  delimiter  = "${var.delimiter}"
  attributes = "${var.attributes}"
  tags       = "${var.tags}"
}

resource "aws_iam_role" "default" {
  count              = "${var.enabled == "true" ? 1 : 0}"
  name               = "${module.label.id}-codebuild-role"
  assume_role_policy = "${data.aws_iam_policy_document.role.json}"
}

data "aws_iam_policy_document" "role" {
  statement {
    sid = ""

    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type        = "Service"
      identifiers = ["codebuild.amazonaws.com"]
    }

    effect = "Allow"
  }
}

resource "aws_iam_policy" "default" {
  count  = "${var.enabled == "true" ? 1 : 0}"
  name   = "${module.label.id}-default-policy"
  path   = "/service-role/"
  policy = "${data.aws_iam_policy_document.permissions.json}"
}

# resource "aws_iam_policy" "assume_role" {
#   count  = "${(length(var.assumed_role_list) > 0) ? 1 : 0}"
#   name   = "${module.label.id}-assume-role-policy"
#   path   = "/service-role/"
#   policy = "${data.aws_iam_policy_document.assume_role.json}"
# }

resource "aws_iam_policy" "default_ecs" {
  count  = "${var.ecs_enabled == "true" ? 1 : 0}"
  name   = "${module.label.id}-ecs-policy"
  path   = "/service-role/"
  policy = "${data.aws_iam_policy_document.permissions_ecs.json}"
}

resource "aws_iam_policy" "default_ssm" {
  count  = "${var.ssm_enabled == "true" ? 1 : 0}"
  name   = "${module.label.id}-ssm-policy"
  path   = "/service-role/"
  policy = "${data.aws_iam_policy_document.permissions_ssm.json}"
}

# data "aws_iam_policy_document" "assume_role" {
#   count = "${signum(length(var.assumed_role_list))}"

#   statement {
#     sid = "AssumeRole"

#     actions = [
#       "sts:AssumeRole",
#     ]

#     resources = "${var.assumed_role_list}"
#   }
# }

data "aws_iam_policy_document" "permissions" {

  statement {
    sid = ""

    actions = [
      "sts:AssumeRole",
      "iam:GetPolicy",
      "iam:GetPolicyVersion",
    ]

    resources = [
      "*",
    ]
  }

  statement {
    sid = ""

    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]

    resources = [
      "*",
    ]
  }
  statement {
    sid = ""

    actions = [
      "s3:ListBucket",
    ]

    resources = [
      "arn:aws:s3:::${var.tf_backend_s3_bucket}",
    ]
  }

  statement {
    sid = ""

    actions = [
      "s3:GetObject",
      "s3:PutObject",
    ]

    resources = [
      "arn:aws:s3:::${var.tf_backend_s3_bucket}/${module.label.id}/${var.stage}/*",
    ]
  }

  statement {
    sid = ""

    actions = [
      "dynamodb:GetItem",
      "dynamodb:PutItem",
      "dynamodb:DeleteItem",
    ]

    resources = [
      "arn:aws:dynamodb:${local.aws_region}:${local.aws_account_id}:table/${var.tf_dynamodb_table}",
    ]
  }
}

data "aws_iam_policy_document" "permissions_ecs" {
  count = "${var.ecs_enabled == "true" ? 1 : 0}"

  statement {
    sid = ""

    actions = [
      "ecr:BatchCheckLayerAvailability",
      "ecr:CompleteLayerUpload",
      "ecr:GetAuthorizationToken",
      "ecr:InitiateLayerUpload",
      "ecr:PutImage",
      "ecr:UploadLayerPart",
      "codecommit:BatchGetRepositories",
      "codecommit:Get*",
      "codecommit:GitPull",
      "codecommit:List*",
      "codecommit:Put*",
      "codecommit:Test*",
      "codecommit:Update*",
    ]

    effect = "Allow"

    resources = [
      "*",
    ]
  }
}

data "aws_iam_policy_document" "permissions_ssm" {
  count = "${var.ssm_enabled == "true" ? 1 : 0}"

  statement {
    sid = ""

    actions = [
      "ssm:PutParameter",
      "ssm:DeleteParameter",
      "ssm:DescribeParameters",
      "ssm:GetParameterHistory",
      "ssm:GetParametersByPath",
      "ssm:GetParameters",
      "ssm:GetParameter",
      "ssm:DeleteParameters",
    ]

    resources = [
      "arn:aws:ssm:${local.aws_region}:${local.aws_account_id}:parameter/codebuild/*",
      "arn:aws:ssm:${local.aws_region}:${local.aws_account_id}:parameter/${var.name}/${var.stage}/*",
    ]
  }

  statement {
    sid = "KMSListAliases"

    actions = [
      "kms:ListAliases",
    ]

    resources = ["*"]
  }

  statement {
    sid = "KMSDecrypt"

    actions = [
      "kms:Decrypt",
    ]

    resources = [
      "arn:aws:kms:${var.aws_region}:${local.aws_account_id}:alias/aws/ssm",
    ]
  }
}

data "aws_iam_policy" "AdministratorAccess" {
  count = "${var.admin_access_enabled == "true" ? 1 : 0}"
  arn   = "arn:aws:iam::aws:policy/AdministratorAccess"
}

# TODO: Replace role `${var.stage}_ansibleAdministrator` with CodeBuild specific admin role.
resource "aws_iam_role_policy_attachment" "admin" {
  count      = "${var.admin_access_enabled == "true" ? 1 : 0}"
  policy_arn = "${data.aws_iam_policy.AdministratorAccess.arn}"
  role       = "${aws_iam_role.default.id}"
}

resource "aws_iam_role_policy_attachment" "default" {
  count      = "${var.enabled == "true" ? 1 : 0}"
  policy_arn = "${aws_iam_policy.default.arn}"
  role       = "${aws_iam_role.default.id}"
}

# resource "aws_iam_role_policy_attachment" "assume_role" {
#   count      = "${signum(length(var.assumed_role_list))}"
#   policy_arn = "${element(aws_iam_policy.assume_role.*.arn, count.index)}"
#   role       = "${aws_iam_role.default.id}"
# }

resource "aws_iam_role_policy_attachment" "default_ssm" {
  count      = "${var.enabled == "true" && var.ssm_enabled == "true" ? 1 : 0}"
  policy_arn = "${element(aws_iam_policy.default_ssm.*.arn, count.index)}"
  role       = "${aws_iam_role.default.id}"
}

resource "aws_iam_role_policy_attachment" "default_ecs" {
  count      = "${var.enabled == "true" && var.ecs_enabled == "true" ? 1 : 0}"
  policy_arn = "${element(aws_iam_policy.default_ecs.*.arn, count.index)}"
  role       = "${aws_iam_role.default.id}"
}

resource "aws_codebuild_project" "default" {
  count        = "${var.enabled == "true" ? 1 : 0}"
  name         = "${module.label.id}"
  service_role = "${aws_iam_role.default.arn}"

  artifacts {
    type = "NO_ARTIFACTS"
  }

  environment {
    compute_type = "${var.build_compute_type}"
    image        = "${var.build_image}"
    type         = "LINUX_CONTAINER"

    environment_variable = [
      {
        "name"  = "STAGE"
        "value" = "${signum(length(var.stage)) == 1 ? var.stage : "UNSET"}"
      },
      "${var.environment_variables}",
    ]
  }

  source {
    type            = "${var.source_type}"
    location        = "${var.source_location}"
    buildspec       = "${var.buildspec}"
    git_clone_depth = 1
  }

  tags = "${var.tags}"
}
