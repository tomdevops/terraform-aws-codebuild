output "project_name" {
  value = "${join("", aws_codebuild_project.default.*.name)}"
}

output "project_id" {
  value = "${join("", aws_codebuild_project.default.*.id)}"
}

output "role_arn" {
  value = "${join("", aws_iam_role.default.*.id)}"
}
