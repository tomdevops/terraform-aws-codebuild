variable "namespace" {
  type    = "string"
  default = ""
}

variable "stage" {
  type = "string"
}

variable "name" {
  type    = "string"
  default = "codebuild"
}

variable "environment_variables" {
  type = "list"

  default = [{
    "name"  = "NO_ADDITIONAL_BUILD_VARS"
    "value" = "TRUE"
  }]

  description = "A list of maps, that contain both the key 'name' and the key 'value' to be used as additional environment variables for the build."
}

# NOTE: Set to `true` for now, because we dont know what AWS resources are created using TF and SLS ... could be anything
# To make life a bit easier: I just add the `AdministratorAccess` privs to the CodeBuild Role.
variable "admin_access_enabled" {
  type        = "string"
  default     = "true"
  description = "A boolean to enable/disable Administrator access for CodeBuild service IAM role (to deploy using TF and SLS)"
}

variable "enabled" {
  type        = "string"
  default     = "true"
  description = "A boolean to enable/disable resource creation."
}

variable "ecs_enabled" {
  type        = "string"
  default     = "false"
  description = "A boolean to enable/disable access to ECS/ECR resources"
}

variable "ssm_enabled" {
  type        = "string"
  default     = "true"
  description = "A boolean to enable/disable access to the SSM Parameter Store"
}

variable "ssm_prefix" {
  type        = "string"
  default     = ""
  description = ""
}

variable "cache_enabled" {
  type        = "string"
  default     = "true"
  description = "If cache_enabled is true, create an S3 bucket for storing codebuild cache inside"
}

variable "cache_expiration_days" {
  type        = "string"
  default     = "7"
  description = "How many days should the build cache be kept."
}

variable "cache_bucket_suffix_enabled" {
  type        = "string"
  default     = "true"
  description = "The cache bucket generates a random 13 character string to generate a unique bucket name. If set to false it uses terraform-null-label's id value"
}

variable "build_image" {
  type        = "string"
  default     = "aws/codebuild/nodejs:8.11.0"
  description = "Docker image for build environment, e.g. 'aws/codebuild/nodejs:8.11.0' or 'aws/codebuild/docker:17.09.0'. For more info: http://docs.aws.amazon.com/codebuild/latest/userguide/build-env-ref.html"
}

variable "build_compute_type" {
  type    = "string"
  default = "BUILD_GENERAL1_SMALL"
}

variable "delimiter" {
  type    = "string"
  default = "-"
}

variable "attributes" {
  type    = "list"
  default = []
}

variable "tags" {
  type    = "map"
  default = {}
}

variable "aws_region" {
  type        = "string"
  default     = ""
  description = "(Optional) AWS Region, e.g. us-east-1. Used as CodeBuild ENV variable when building Docker images. For more info: http://docs.aws.amazon.com/codebuild/latest/userguide/sample-docker.html"
}

variable "aws_account_id" {
  type        = "string"
  default     = ""
  description = "(Optional) AWS Account ID. Used as CodeBuild ENV variable when building Docker images. For more info: http://docs.aws.amazon.com/codebuild/latest/userguide/sample-docker.html"
}

variable "source_type" {
  type        = "string"
  description = ""
}

variable "source_location" {
  type        = "string"
  description = ""
}

variable "buildspec" {
  type        = "string"
  description = "The build spec declaration to use for this build project's related builds"
  default     = "buildspec.yml"
}

variable "assumed_role_list" {
  type        = "list"
  default     = []
  description = "(Optional) List of roles that can be assumed by CodeBuild. Used for cross-account deployment."
}

#
# Terraform stuff
#
variable "tf_backend_s3_bucket" {
  type        = "string"
  default     = "terraform.admin.mkb-brandstof.nl"
  description = "(Required) The name of the S3 bucket where Terraform stores its state."
}

variable "tf_dynamodb_table" {
  type        = "string"
  default     = "terraform-lock"
  description = "(Required) The name of a DynamoDB table to use for state locking and consistency."
}
